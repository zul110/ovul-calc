const ovulDaySelector = $('#ovul-calc-day-selector');
const ovulMonthSelector = $('#ovul-calc-month-selector');
const ovulYearSelector = $('#ovul-calc-year-selector');
const numDaysSelector = $('#ovul-calc-num-days-selector');

const ovulStartContainer = $('#ovul-calc-result-ovul-start-container');
const ovulEndContainer = $('#ovul-calc-result-ovul-end-container');

const ovulSubmitButton = $('#btn_Submit_ovulCalc_Right');
const ovulResultButton = $('#ovul-calc-result-return-button');
const ovulDates = [];

const ovulRefreshTime = 100;

const ovulCalcInit = () => {
    ovulDaySelector.val(0).change();
    ovulMonthSelector.val(0).change();
    ovulYearSelector.val(0).change();

    ovulMonthSelector.prop('disabled', true);
    ovulDaySelector.prop('disabled', true);

    const ovulUpdateYears = () => {
        const today = moment().format('YYYY');
        const lastYear = moment().subtract(1, 'year').format('YYYY');

        const years = [today, lastYear];

        ovulYearSelector.innerHTML = years.forEach(year => ovulYearSelector.append(new Option(year, year)));
    };

    const ovulUpdateMonths = () => {
        const months = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
        ];

        ovulMonthSelector.innerHTML = months.forEach(month => ovulMonthSelector.append(new Option(moment(month, 'M').format('MMMM'), month)));
    };

    const ovulUpdateDays = () => {
        const days = [];
        const numDaysInMonth = moment(`${ovulYearSelector.val()}-${ovulMonthSelector.val()}`, 'YYYY-MM').daysInMonth();

        let day = 1;
        while (day <= numDaysInMonth) {
            days.push(day++);
        }

        ovulDaySelector.innerHTML = days.forEach(day => ovulDaySelector.append(new Option(day, day)));
    };

    ovulUpdateYears();
    ovulUpdateMonths();

    ovulYearSelector.on('change', e => {
        const year = e.target.value;
        const month = ovulMonthSelector.val();

        let _year = year;

        ovulMonthSelector.prop('disabled', year == 0);
        ovulDaySelector.prop('disabled', year == 0 || month == 0);
    });

    ovulMonthSelector.on('change', e => {
        const year = ovulYearSelector.val();
        const month = e.target.value;

        let _month = month;

        ovulUpdateDays();

        ovulDaySelector.prop('disabled', year == 0 || month == 0);
    });

    ovulDaySelector.on('change', e => {
        const day = ovulDaySelector.val();

        let _day = day;
    });
};

const initOvulCalendar = () => {
    const windowWidth = $(window).width();

    calendar.datepicker({
        onChangeMonthYear: () => {
            setTimeout(() => ovulStyleCalendar(), ovulRefreshTime);
        }
    });

    calendar.datepicker("option", "numberOfMonths", windowWidth >= 480 ? 2 : 1);
};

function ovulGetDateStatus(date) {
    for (let i = 0; i < ovulDates.length; i++) {
        if (ovulDates[i].from <= date && ovulDates[i].to >= date) {
            return 'ovul';
        }
    }

    for (let i = 0; i < ovulDates.length; i++) {
        if (ovulDates[i].from <= date && ovulDates[i].to >= date) {
            return 'ovul';
        }
    }

    return 'none';
}

const ovulStyleCalendar = () => {
    $(".ui-datepicker-calendar a").each(function() {
        const _this = $(this);
        const parent = _this.parent();
        const month = Number(parent.data('month'));
        const year = Number(parent.data('year'));
        const day = Number(_this.text());

        const date = new Date(year, month, day);
        const dateStatus = ovulGetDateStatus(date);
        let cls = '';

        if (dateStatus === 'ovul') {
            cls = 'ovul-day';
        } else if (dateStatus === 'ovul') {
            cls = 'ovul-day';
        }

        _this.addClass(cls);
    });
}

ovulSubmitButton.click(() => {
    const firstDay = moment(`${ovulDaySelector.val()}-${ovulMonthSelector.val()}-${ovulYearSelector.val()}`, 'DD-MM-YYYY');
    const lastDayPrev = moment(firstDay).subtract(30, 'days');
    const lastOvulStart = moment(lastDayPrev).add(12, 'days');
    const nextOvulStart = moment(lastOvulStart).add(numDaysSelector.val() - 1, 'days');
    const nextOvulEnd = moment(nextOvulStart).add(5, 'days');

    ovulStartContainer.html(nextOvulStart.format('DD/MM/YYYY'));
    ovulEndContainer.html(nextOvulEnd.format('DD/MM/YYYY'));

    ovulShowResults();
});

ovulResultButton.click(() => {
    ovulHideResults();
});

const ovulHideResults = () => {
    $('.ovul-calc-result').css('display', 'none');
    $('.calcWzDescCnts').css('display', 'block');
};

const ovulShowResults = () => {
    $('.ovul-calc-result').css('display', 'flex');
    $('.calcWzDescCnts').css('display', 'none');
};